 <?php
include "config.php";


	
	?>
	<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<title> PZPN</title>
			<!-- Skrypty i linki do bootstrap oraz jquery -->
		  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

		<!--  -->


<script type="text/javascript">
		
  function profil(id)    {
        $('#prof').dialog({
            modal: true,
            open: function ()
            {
               
 $(this).empty().load('produkty_edit.php?id='+id);
                
            },     
 
        
            height: 400,
            width: 400,
            title:"Edytuj Produkt"
        });
    };

    

	
		$(document).ready(function(){
			
			
			
			
    	    $("#txtSearch").on("keyup", function() {
    	//alert("asd");
			var value = $(this).val();

			$("#table tr").each(function(index) {
				if (index !== 0) {

					$(this).find("td").each(function () {
						var text = $(this).text().toLowerCase().trim();
						var not_found = (text.indexOf(value) == -1);
						$(this).closest('tr').toggle(!not_found);
						return not_found;
					});
				}
			});
		});

    });
	

		/*Ustawienie wykonania działań wówczas, gdy strona jest całkowicie wczytana */
	$(document).ready(function(){
     
    /*WYSYŁANIE DANYCH DO BAZY*/
		$('#dodaj_produkt').click(function() { /*Zdefiniowanie zdarzenia inicjującego 
    - kliknięcie przycisku wyślij*/
        /*Funkcja pobierająca wartość opcji z listy, w tym przypadku nazwa kraju, 
        która następnie zapisywana jest do zmiennej*/
			var nazwa=$("#nazwaAdd").val();
			var cena=$("#cenaAdd").val();
			var cena1=$("#cena1Add").val();
			var jednostka=$("#jednostkaAdd").val();
			
			
			var dataString = 'nazwa='+nazwa+'&cena='+cena+'&cena1='+cena1+'&jednostka='+jednostka;
			if($.trim(nazwa).length>0 && $.trim(cena).length>0)
			//var wartosc_z_listy = 'username='+username+'nazwisko='+nazwisko;
			{
        $.ajax({
            type:"POST", /*Informacja o tym, że dane będą wysyłane*/
            url:"b.php", /*Informacja, o tym jaki plik będzie przy tym wykorzystywany*/
              data: dataString ,/*Zdefiniowanie jakie dane będą wysyłane na zasadzie 
            pary klucz-wartość np: wartosc_z_listy_ajax=Polska*/
        
                /*Działania wykonywane w przypadku sukcesu*/
                success:function() {
                    //Zdefiniowanie tzw. alertu (prostej informacji) w sytacji sukcesu wysyłania. 
                    alert("Wysłano do bazy danych"); 
					 $('#nazwaAdd').val('');
					 $('#cenaAdd').val('');
					 $('#cena1Add').val('');
					 $('#jednostkaAdd').val('');
					 
      $('#table').load('produkty.php #tabela', function(){
           setTimeout(refreshTable, 5000);
        });
    

					
                },
                /*Działania wykonywane w przypadku błędu*/
                error: function(blad) {
                    alert( "Wystąpił błąd");
                    console.log(blad); /*Funkcja wyświetlająca informacje  o ewentualnym błędzie w konsoli przeglądarki*/
                }
        });
		
			}
			else {
				alert("Wprowadz dane")
			}
    });
 });
 </script>
 </head>
<body>
  <div id="prof"></div>


<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#lista" role="tab">Lista Produktów</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#produkt" role="tab">Dodaj Produkt</a>
  </li>
 <!-- <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#messages" role="tab">Messages</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a>
  </li> -->
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div class="tab-pane active" id="lista" role="tabpanel">
	  
	<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h4> Produkty </h4>

	 		
	 		<div class="input-group" style="padding-top:10px;">
				<span class="input-group-addon "><i class=" glyphicon glyphicon-search" ></i></span>
				<input class="form-control"type="text" name="haslo" id="txtSearch" style="color:black;" placeholder="Podaj szukane hasło"  />
			</div>



</div>

  <!-- Table -->
  <div id="tabela" class="table-responsive">

  <table class="table" id="table">
   
   <thead>
      <tr>
        <th width="5%">#</th>
        <th width="20%"> Nazwa Produktu</th>
        <th width="15%"> Cena Netto</th>
		<th width="15%"> Cena Brutto</th>
        <th width="20%"> Jednostka Miary</th>
		<th > Opcje</th>
      </tr>
    </thead>
	 <tbody>
      
<?php 

	
	$username = $_SESSION["name"];
	
	$sql = "SELECT * FROM produkty  ";
	
	$run_query = mysqli_query($con,$sql);
	
	while($row = mysqli_fetch_assoc($run_query)) {
?>
	<div class='id' id='id_<?php echo $row['id_produktu']; ?>'>  
	<tr>
   <td><?php echo $row['id_produktu']; ?></td>  
   <td><?php echo $row['nazwa_produktu']; ?></td>  
   <td><?php echo $row['cena_netto']; ?></td>  
   <td><?php echo $row['cena_brutto']; ?></td>  
   <td><?php echo $row['jednostka_miary']; ?></td> 
	<td> <a class="glyphicon glyphicon-trash" href="#" id="usun"></a> 
	
	<a class="glyphicon glyphicon-edit" href="#" onclick="profil(<?php echo $row['id_produktu']; ?>)" id="edytuj"></a>  
<!--<ul class="nav navbar-nav navbar-right">
<li><a href="#" id="cart_container" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span>Cart<span class="badge">0</span></a>
					<div class="dropdown-menu" style="width:400px;">
						<div class="panel panel-success">
							<div class="panel-heading">
								<div class="row">
									<div class="col-md-3 col-xs-3">Sl.No</div>
									<div class="col-md-3 col-xs-3">Product Image</div>
									<div class="col-md-3 col-xs-3">Product Name</div>
									<div class="col-md-3 col-xs-3">Price in $.</div>
								</div>
							</div>
							<div class="panel-body">
								<div id="cart_product">
								<!--<div class="row">
									<div class="col-md-3">Sl.No</div>
									<div class="col-md-3">Product Image</div>
									<div class="col-md-3">Product Name</div>
									<div class="col-md-3">Price in $.</div>
								</div
								</div>
							</div>
							<div class="panel-footer"></div>
						</div>
					</div>
</ul>-->

	</td>   
</tr>
  </div>  
<?php 
	}
	
			

 ?>  

    </tbody>
  </table>
  </div>
</div> 
	  
	  
	</div>
	
	<div class="tab-pane" id="produkt" role="tabpanel">
	  
	  <div class="panel panel-default">
			<div class="panel-heading"><h4>Dodaj Produkt</h4></div>
		<div  class="panel-body">
			<form  > 

				<div class="from-group">
					<div class="input-group " style="margin-top:5px;" >
						<span class="input-group-addon" >Nazwa Produktu </span>
						<input type="text" class="form-control" id="nazwaAdd" name="nazwaAdd" >
					</div>	
				</div>
				
				<div class="from-group">
					<div class="input-group " style="margin-top:5px;">
						<span class="input-group-addon" >Cena Produktu Netto</span>
						<input type="text" class="form-control" id="cenaAdd" name="cenaAdd" >
						<span class="input-group-addon" id="basic-addon2">zł</span>
					</div>	
				</div>
				
				<div class="from-group">
					<div class="input-group " style="margin-top:5px;">
						<span class="input-group-addon" >Cena Produktu Brutto</span>
						<input type="text" class="form-control" id="cena1Add" name="cena1Add" >
						<span class="input-group-addon" id="basic-addon2">zł</span>
					</div>	
				</div>
				
				<div class="from-group">
					<div class="input-group " style="margin-top:5px;">
					<span class="input-group-addon" >Jednostka miary </span>
						<select class="form-control" id="jednostkaAdd" name="jednostkaAdd">
						  <option selected></option>
						  <option value="m">m</option>
						  <option value="cm">cm</option>
						  <option value="szt">szt</option>
						</select>
					</div>	
				</div>
						
				
						<div   style="text-align:center; margin-top:30;">
				<button type="button" class="btn btn-success" id="dodaj_produkt">Dodaj</button>
				</div>	
				</form>

		</div>
	
	</div>
  
  </div>
 
</div>
</body>
</html>
<?php 
include "config.php";
session_start();
if(!isset($_SESSION['username'])){
	header('location: login.php');
//header("location: user.php");
}

?>



<html lang="pl">
<head>
	<meta charset="UTF-8" />
		<title>Faktury</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title></title>
	
	<!-- jQuery -->
	<script src="js/jquery-1.11.3.min.js"></script>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="bootstrap-3.3.2/css/bootstrap.min.css">

	<!-- menu -->
	<link href="metisMenu/dist/metisMenu.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="css/sb-admin-2.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


<script type="text/javascript">

		$(document).ready( function() {
    $("#user").on("click", function() {
        $("#main").load("user.php #addUser");
    });
});
			
			</script>

</head>
<body>

	
	<div id="wrapper">
		<nav id="navbar" class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="" onclick="loadPage('./statystyki.php')">FAKTURY</a>		
			</div>


			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['imie']; ?>  <i class="fa fa-caret-down"></i>
					</a>
					<ul  class="dropdown-menu dropdown-user in">
						
						<li id="userMenu" class="divider"></li>
						<li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Wyloguj</a>
						</li>
					</ul>
					<!-- /.dropdown-user -->
				</li>
			</ul>	

			<!-- dropdown menu -->

			<!-- menu-toggle -->
			<div class="navbar-default sidebar" role="navigation">
				<div id="menuContent" class="sidebar-nav navbar-collapse">
					
					<ul class="nav" id="side-menu">
				<li><a href="#user" id="user"><i class="fa fa-table fa-fw"></i> Moje Dane </a></li>
				<li ><a href="#panel#listaklientow" onclick="loadDiv('listaKlientow');"><i class="fa fa-users fa-fw"></i> Lista Klientów</a></li>
					</ul>

					
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>
		
		<!-- okno -->
		<div id="page-wrapper">
		
		<div id="infoBar" class="alert alert-success" style="display: none;"></div>
			<div class="row">
				<div class="col-lg-12">
					<div id="main"></div>
					
				</div>
				<!-- /.col-lg-12 -->
			</div>

		</div>
	</div>
	<script type="text/javascript" src="bootstrap-3.3.2/js/bootstrap.min.js"></script>
	
	
</body>
</html>
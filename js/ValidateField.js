/*
format modelu form :
<div class="from-group">
	<div class="input-group " style="margin-top:15px;">
		<input type="email" class="form-control" id="emailAdd" name="emailAdd" placeholder="Email">
	</div>
		<label class="label" for="emailAdd"></label> 
</div>

funkcja php musi zwracac 1 true 0 false na wyjsciu  
*/
(function( $ ) {

	$.fn.validate = function(options) {
		ValidateField(this,options);
	};


	function ValidateField(formId,options){
		var root = this;
		var vars = {
			formId:'',
			email:'',
			textButton: '',
			optionAjax:'',
			checkEmailInDb: false,
			fileExecute: '',
			textRegex: /^[a-żA-Ż]{3,43}$/,
			passwordRegex: (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{7,}$/),
			labels: {
				emailLabel: "Zły format email, np: nowak@pw.pl",
				emailIsInDBLabel: "Podany email istnieje w bazie",
				passwordRegexLabel: "Hasło musi posiadć: ( min 8 znaków, 1-cyfrę, 1-dużą literę, 1-małą literę )",
				passwordDiffLabel: "Hasła są różne",
				textLabel:"Minimalnie 3 litery, maksymalnie 43 litery",
				emptyLabel:"Pole nie może być pustę",
				listEmpty:"Wybierz opcję",
			}//labels
		}//vars

		//this.formId = formId;
		this.emaiRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		this.passwordConfrimId="";
		this.passwordId="";

		this.construct = function(options){
			$.extend(vars , options);
			this.init();
		}//construct

		
		
		this.responseAjax = function(putUrl,data,callback) {

			return $.ajax({
				url: putUrl, 
				type: 'POST',
				data: data,
				async: false

			}).done(callback).fail(function(jqXHR, textStatus, errorThrown) {
				alert("Status: " + textStatus); alert("Error: " + errorThrown); 		
			});
		}//response

		this.rSaE = function(elementId,textLabel){
			$(elementId).parent().parent().removeClass('has-success').addClass('has-error');
			$(elementId).parent().parent().children('.label').removeClass('label-success')
			.addClass('label-danger').text(textLabel);
		}//rsae

		this.rEaS = function(elenentId){
			$(elenentId).parent().parent().removeClass('has-error').addClass('has-success');
			$(elenentId).parent().parent().children('.label').text("");	
		}//reas

		this.emailValid = function(inputID){

			$(inputID).change(function() { //przy wyjsciu z pola
				$(this).filter(function(){ // sprawdzanie filtru
					var emil = $(this).val(); //pobieranie warosci z pola o id #password1Add
					var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 

					if( !emailReg.test(emil)) {
						root.rSaE(this,vars.labels.emailLabel);
					} else {
						if(vars.checkEmailInDb){
							$(this).parent().parent().children('.label').text("");
							$(this).parent().parent().append('<div id="load">sprawdzam... <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></div>');

							var dataString = 'option=EMAIL&data='+$(this).val();


							root.responseAjax(vars.fileExecute, dataString,function(data){
								if(data == 1)
								{ 
									$('#load').remove();
									root.rSaE(inputID,vars.labels.emailIsInDBLabel);
								}else {  
									$('#load').remove();
									root.rEaS(inputID);
								} 
							});
						}else{
							root.rEaS(inputID);
						}
					}
				});
			});
		}//email valid

		this.passwordValid = function(inputID){
			this.passwordId = inputID;
			$(inputID).keyup(function(){ //przy wyjsciu z pola
				$(this).filter(function(){ // sprawdzanie filtru
					var passVal = $(this).val(); //pobieranie warosci z pola o id #password1Add
					var passReg = vars.passwordRegex; //regex dla hasła 
					if( !passReg.test( passVal ) ) {
						root.rSaE(this,vars.labels.passwordRegexLabel);
					} else {
						var confrimVal = $(root.passwordConfrimId).val();
						if(passVal === confrimVal){
							root.rEaS(this);
							root.rEaS(root.passwordConfrimId);
						}else{
							root.rSaE(this,"Hasła są różne");
							root.rSaE(root.passwordConfrimId,vars.labels.passwordDiffLabel);
						}		
					}
				});
			});
		}//password valid

		this.passwordValidConfrim = function(inputID){
			this.passwordConfrimId = inputID;
			$(inputID).keyup(function(){ 
			$(this).filter(function(){ // sprawdzanie filtru
				var passVal = $(this).val(); 
				var passConf = $(root.passwordId).val();
				var passReg = vars.passwordRegex;

				if(passReg.test(passConf)){

					if( passConf == passVal ) {
						root.rEaS(this);
						root.rEaS(root.passwordId);
					} else {
						root.rSaE(this,vars.labels.passwordDiffLabel);
						root.rSaE(root.passwordId,vars.labels.passwordDiffLabel);
					}
				}else{
					root.rSaE(this,passwordRegexLabel);
					root.rSaE(root.passwordId,passwordRegexLabel);
				}
			});
		});
		}//password confrim valid

		this.textValid = function(inputID){
			$(inputID).keyup(function(){
				$(this).filter(function(){
					var reg = vars.textRegex;
					var val = $(this).val();
					if(!reg.test( val )){
						root.rSaE(this,vars.labels.textLabel);
					}else{
						root.rEaS(this);
					}
				});
			});
		}//text valid

		this.selectValid = function(selectID){
			$(selectID).on('change', function() {
				if(this.value == 0){

					root.rSaE(this,vars.labels.listEmpty);
				}else{
					root.rEaS(this);		
				}

			});

		}


		this.execute = function(buttonID){

			$(buttonID).click(function() {
				var allFilled = [];
				//alert(JSON.stringify(vars.formId));

				$(vars.formId).find('select').each(function(index, element) {

					if(element.value == 0){
						root.rSaE(this,vars.labels.listEmpty);
						allFilled.push(0);
					}else{
						root.rEaS(this);
						allFilled.push(1);
					}
				});


				
				$(vars.formId).find(':input:not(:button,:checkbox,select)').each(function(index, element) {
					if (element.value === '') {
						root.rSaE(this,vars.labels.emptyLabel);
						allFilled.push(0);
					}else{
						if($(this).parent().parent().closest('.has-success').length==1){
							root.rEaS(this);
							allFilled.push(1);

						}else{
							allFilled.push(0);
						}

					}
				});



				//alert(allFilled);
				var isZero = allFilled.indexOf(0);

				if(isZero == -1){
					var dataString ="option="+vars.optionAjax+"&data="+vars.email+"&";

					$(vars.formId).find(':input:not(:button)').each(function(index, element) {
							dataString += element.id+"="+element.value+"&";
						});

					//alert(dataString);

					
					$.ajax({
						type: "POST",
						url: vars.fileExecute,
						dataType: "html",
						data: dataString,
						cache: false,
						beforeSend: function(){ 
							$(buttonID).text(vars.textButton+' ').append('<i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>');

						},
						success: function(data){
							
							$(buttonID).text(vars.textButton);
							//alert(data);
							if(data)
							{

								switch(vars.textButton){
									case "Dodaj":
									$('div#infoBar').text("Dodano użytkownika").slideDown("slow").delay( 900 );
									$('div#infoBar').slideUp("slow");


									$(vars.formId).find(':input:not(:button,:checkbox,select)').each(function(index, element) {
										element.value = '';
										$(this).parent().parent().removeClass('has-success').removeClass('has-error');
										$(this).parent().parent().children('.label').text("");

									});

									$(vars.formId).find('select').each(function(index, element) {
										element.value = '0';
										if(element.id=='departmentSelect'){
											$(this).parent().parent().remove();
										}
										$(this).parent().parent().removeClass('has-success').removeClass('has-error');
										$(this).parent().parent().children('.label').text("");
									});

									break;
									case "Zapisz":
									$('div#infoBar').text("Edytowano użytkownika").slideDown("slow").delay( 900 );
									$('div#infoBar').slideUp("slow");
									break;
									case "Zmień":
									$('div#infoBar').text("Pomyślnie zmieniono hasło").slideDown("slow").delay( 900 );
									$('div#infoBar').slideUp("slow");
									$(vars.formId).find(':input:not(:button,:checkbox,select)').each(function(index, element) {
										element.value = '';
										$(this).parent().parent().removeClass('has-success').removeClass('has-error');
										$(this).parent().parent().children('.label').text("");

									});
									break;
								}
								


							}else{
								alert(data);
							}

						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							$(buttonID).text(vars.textButton);
							alert("Statusadd: " + textStatus); alert("Errordod: " + errorThrown); 
						}       
					});	 		

}

});
		}//execute

		this.init = function(){

			$(vars.formId).find(':input,select').each(function(index, element) {


				switch(element.type){
					case 'email':
					root.emailValid('#'+element.id);
					break;
					case 'password':
					//alert(index+" id: "+element.id);
					if(index == 1){
						root.passwordValid('#'+element.id);
					}else if(index == 2){
						root.passwordValidConfrim('#'+element.id);
					}
					break;
					case 'text':
					root.textValid('#'+element.id);
					break;
					case 'select-one':
					root.selectValid('#'+element.id);
					break;
					case 'submit':
					case 'button':
					root.execute('#'+element.id);
					break;

				}
			});
			


		}//init

		this.construct(options);
	}

})( jQuery );
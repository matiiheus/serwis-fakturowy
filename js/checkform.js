function checkForm(form) {
	var allFilled = [];

	$("#"+form).find(':input:not(:button)').each(function(index, element) {
		
		switch(element.type){
			case "select-one":
			if($('#'+element.id).val() == 0){
				rSaE('#'+element.id,"pole nie może być puste");
				allFilled.push(0);
			}else{
				rEaS('#'+element.id);
				allFilled.push(1);
			}
			break;
			default:
			if ($('#'+element.id).val() === '') {
				rSaE('#'+element.id,"pole nie może być puste");
				allFilled.push(0);
			}else{
				rEaS('#'+element.id);
				allFilled.push(1);
			}
			break;
		}
		
	});

	return allFilled.indexOf(0);
}


function rSaE(elementId,textLabel){
	$(elementId).parent().parent().removeClass('has-success').addClass('has-error');
	$(elementId).parent().parent().children('.label').removeClass('label-success')
	.addClass('label-danger').text(textLabel);
}//rsae

function rEaS(elenentId){
	$(elenentId).parent().parent().removeClass('has-error').addClass('has-success');
	$(elenentId).parent().parent().children('.label').text("");	
}//reas
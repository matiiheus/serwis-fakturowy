 <?php
include "config.php";
	?>
	<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<title> PZPN</title>
			<!-- Skrypty i linki do bootstrap oraz jquery -->
		  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript">
		
 

    

	
		$(document).ready(function(){
			
    	    $("#txtSearch2").on("keyup", function() {
    	//alert("asd");
			var value = $(this).val();

			$("#table2 tr").each(function(index) {
				if (index !== 0) {

					$(this).find("td").each(function () {
						var text = $(this).text().toLowerCase().trim();
						var not_found = (text.indexOf(value) == -1);
						$(this).closest('tr').toggle(!not_found);
						return not_found;
					});
				}
			});
		});
    });
	

 </script>
  
</head>
		 
<body>
<div>
<!-- 
<div class="tab-pane active" id="lista" role="tabpanel">
-->
		  
	<div class="panel panel-default">
				  <!-- Default panel contents -->
		<div class="panel-heading"><h4> Klienci </h4>	
			<div class="input-group" style="padding-top:10px;">
				<span class="input-group-addon "><i class=" glyphicon glyphicon-search" ></i></span>
				<input class="form-control" type="text" name="haslo" id="txtSearch2" style="color:black;" placeholder="Podaj szukane hasło"  />
			</div>
		</div>

			  <!-- Table -->
		<div id="tabela" class="table-responsive">

			<table class="table" id="table2">
			   
			   <thead>
				  <tr>
					<th width="5%">#</th>
					<th width="20%"> Imie </th>
					<th width="15%"> Nazwisko</th>
					<th width="15%"> Adres</th>
					<th width="20%"> Kod Pocztowy</th>
					<th > Opcje</th>
				  </tr>
				</thead>
			<tbody>
				  
			<?php 

				$sql = "SELECT * FROM klient  ";
				
				$run_query = mysqli_query($con,$sql);
				
				while($row = mysqli_fetch_assoc($run_query)) {
			?>
				
				<tr>
			   <td><?php echo $row['id_klienta']; ?></td>  
			   <td><?php echo $row['imie_klienta']; ?></td>  
			   <td><?php echo $row['nazwisko_klienta']; ?></td>  
			   <td><?php echo $row['adres_klienta']; ?></td>
			   <td><?php echo $row['kod_pocztowy_klienta']; ?></td>  
			   
			   
				<td> 
				<a class="glyphicon glyphicon-trash" href="#" id="usun"></a> 
				<a class="glyphicon glyphicon-edit" href="#" onclick="profil(<?php echo $row['id_klienta']; ?>)" id="edytuj"></a>  

				</td>   
			</tr>
			 
			<?php 
				}
			 ?>  

			</tbody>
			</table>
		</div>
	</div>  
</div>
</body>
</html>